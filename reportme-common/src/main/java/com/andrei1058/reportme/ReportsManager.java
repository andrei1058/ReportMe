package com.andrei1058.reportme;

import com.andrei1058.reportme.commands.ICommand;
import com.andrei1058.reportme.commands.Info;
import com.andrei1058.reportme.commands.MyReports;
import com.andrei1058.reportme.commands.Report;
import com.andrei1058.reportme.database.Database;
import com.andrei1058.reportme.database.MySQL;
import com.andrei1058.reportme.database.SQLite;
import com.andrei1058.reportme.server.IServerSoftware;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;

public class ReportsManager {

    private IServerSoftware serverSoftware;
    private LinkedList<ICommand> commands = new LinkedList<>();
    public Database database;
    private HashMap<String, Object> defaultMessages = new HashMap<>();
    private LinkedList<UUID> suspectsOnly = new LinkedList<>();

    private static ReportsManager reportsManager = null;


    private ReportsManager(IServerSoftware serverSoftware) {
        this.serverSoftware = serverSoftware;
        reportsManager = this;

        commands.add(new Report());
        commands.add(new Info());
        commands.add(new MyReports());

        for (ICommand c : commands) {
            getServerSoftware().registerCommand(c);
        }
        database = new SQLite();
        if (ReportsManager.getReportsManager().getServerSoftware().getRConfig().isDatabaseEnabled()) new MySQL();

        defaultMessages.put("prefix", "&2[ReportMe] ");
        defaultMessages.put("date-format", "dd/MM/yyyy HH:mm");
        defaultMessages.put("cmd-no-permission", "{prefix}&7You can't do that!");
        defaultMessages.put("cmd-report-usage", Arrays.asList("&8&l> {prefix} &8-------", "&2Usage: &7/report <user> <reason>", "&2Where &7user &2is the player and &7reason &2is your message.", "&2Include a video link if possible."));
        defaultMessages.put("cmd-report-invalid-user", "{prefix}&7{player} has never played on this server.");
        defaultMessages.put("cmd-report-invalid-self", "{prefix}&7You cannot report yourself =)))))");
        defaultMessages.put("cmd-report-invalid-reason", "{prefix}&7Cannot understand this report type :(");
        defaultMessages.put("cmd-report-invalid-length", "{prefix}&7Your message is too short.");
        defaultMessages.put("cmd-report-offline-user", "{prefix}&7{player} is offline!");
        defaultMessages.put("cmd-report-protected", "{prefix}&7This user cannot be reported.");
        defaultMessages.put("cmd-report-success", Arrays.asList("&8&l> {prefix} &8-------", "&7Your report will be reviewed as soon as possible!", "&2You can always see the report status using &7/myreports"));
        defaultMessages.put("cmd-report-success-web", "&2Report Link: &7{link}");
        defaultMessages.put("cmd-report-cooldown", "{prefix}&7You cannot use &2/report &7for &2&o{time} seconds.");
        defaultMessages.put("cmd-report-cooldown-same", "{prefix}&7You can't report this player again for &2&o{time} minutes.");
        defaultMessages.put("cmd-rinfo-usage", "{prefix}&2Usage: &7/rinfo <id>");
        defaultMessages.put("cmd-rinfo-invalid", "{prefix}&7Could not found any report with ID: &2{id}");
        defaultMessages.put("cmd-rinfo-status-open", Arrays.asList("", "&8&l> {prefix} &2ID#&f{id} &8-------  ", "&7 Status: {status}", "&7 Open Date: &f{openDate}", "&7 Offended: &f{reporter}", "&7 Reported: &c{reported} &7&o({activeReports} active reports)",
                "&7 Reason: &c{reason}", ""));
        defaultMessages.put("cmd-rinfo-status-closed", Arrays.asList("", "&8&l> {prefix} &2ID#&f{id} &8-------  ", "&7 Status: {status}", "&7 Open Date: &f{openDate}", "&7 Open Date: &f{closeDate}", "&7 Offended: &f{reporter}", "&7 Reported: &c{reported} &7&o({activeReports} active reports)",
                "&7 Reason: &c{reason}", "&7 Moderator: &b{moderator}", "&c Verdict: &f{verdict}", ""));
        defaultMessages.put("cmd-rinfo-comments-header", "&f#&2{id} &fComments:");
        defaultMessages.put("cmd-rinfo-comments-format-admin", "&7[{date} &b&o{user}&r&7] &f{comment}");
        defaultMessages.put("cmd-rinfo-comments-format-user", "&7[{date} &o{user}&r&7] &f{comment}");
        defaultMessages.put("cmd-myreports-usage", Arrays.asList("&8&l> {prefix} &8-------", "&2Usage: &7/myreports <active/closed> &o<latest/oldest>"));
        defaultMessages.put("cmd-myreports-next-page", "&6Type this command again to see the next page.");
        defaultMessages.put("cmd-myreports-active-list-header", "&f\n{prefix} &2MY ACTIVE REPORTS - Page {page}");
        defaultMessages.put("cmd-myreports-closed-list-header", "&f\n{prefix} &2MY CLOSED REPORTS - Page {page}");
        defaultMessages.put("cmd-myreports-active-list-format", " &f#&2{id} &6&o{unread}&e{reported} &8- {status}");
        defaultMessages.put("cmd-myreports-closed-list-format", " &f#&2{id} &6&o{unread}&e{reported} &8- {status}");
        defaultMessages.put("cmd-myreports-active-hover-format", Arrays.asList("&2Open date: &f{openDate}", "&2Reason: &f{reason}", "&fClick for details."));
        defaultMessages.put("cmd-myreports-closed-hover-format", Arrays.asList("&2Close date: &f{closeDate}", "&2Reason: &f{reason}", "&2Moderator: &f{moderator}", "&2Verdict: &f{verdict}", "&fClick for details."));
        defaultMessages.put("nothing-to-show", "{prefix} &7Nothing to show.");
        //todo la bungee sa fie subtitlu cu server -> user
        defaultMessages.put("moderator-notify-title", "&2New report");
        // {reporter} {reported}
        defaultMessages.put("moderator-notify-subtitle", "&c{reported}");
        defaultMessages.put("moderator-notify-action", "{prefix}");
        defaultMessages.put("moderator-notify-chat", Arrays.asList("", "&8&l> {prefix} &8-------", "&7 Reporter: &a{reporter}", "&7 Reported: &c{reported}", "&2 Reason: &7{reason}", ""));
        defaultMessages.put("user-notify-subtitle", "&aReport sent!");
        defaultMessages.put("placeholder-status-0", "&8Closed");
        defaultMessages.put("placeholder-status-1", "&cOpen");
        defaultMessages.put("placeholder-status-2", "&6Moderator Answer");
        defaultMessages.put("placeholder-status-3", "&6Offended Answer");
        defaultMessages.put("placeholder-status-4", "&8Closed By Offended");
        defaultMessages.put("placeholder-moderator", "&7None");
        defaultMessages.put("placeholder-unread", "[NEW MESSAGE] ");
    }

    public HashMap<String, Object> getDefaultMessages() {
        return defaultMessages;
    }

    public IServerSoftware getServerSoftware() {
        return serverSoftware;
    }

    public LinkedList<ICommand> getCommands() {
        return commands;
    }

    /**
     * Get singleton.
     */
    public static ReportsManager getReportsManager() {
        return reportsManager;
    }

    public LinkedList<UUID> getSuspectsOnly() {
        return suspectsOnly;
    }

    /**
     * Initialize the reports manager.
     */
    public static void init(IServerSoftware serverSoftware) {
        if (reportsManager == null) new ReportsManager(serverSoftware);
    }
}
