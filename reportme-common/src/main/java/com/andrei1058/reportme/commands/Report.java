package com.andrei1058.reportme.commands;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.ReportsManager;
import com.andrei1058.reportme.server.IServerSoftware;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

public class Report implements ICommand {

    private static HashMap<UUID, Long> cooldown = new HashMap<>();

    private static HashMap<UUID, HashMap<UUID, Long>> sameReportCooldown = new HashMap<>();

    @Override
    public String getName() {
        return "report";
    }

    @Override
    public void execute(IUser sender, String[] args) {
        IServerSoftware sw = ReportsManager.getReportsManager().getServerSoftware();
        if (args.length < 2) {
            sender.sendMessages(sw.getMessages().getMessages(sender, "cmd-report-usage"));
            return;
        }

        if (cooldown.containsKey(sender.getUUID())) {
            if (cooldown.get(sender.getUUID()) > System.currentTimeMillis()) {
                sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-report-cooldown").replace("{time}",
                        new DecimalFormat("#.0").format((double) (cooldown.get(sender.getUUID()) - System.currentTimeMillis()) / 1000)));
                return;
            } else {
                cooldown.remove(sender.getUUID());
            }
        }

        String reason = String.join(" ", Arrays.copyOfRange(args, 1, args.length));

        if (!sw.getRConfig().acceptCustomReason()){
            if (!sw.getRConfig().getReportTypes().contains(reason)){
                sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-report-invalid-reason"));
                return;
            }
        }

        if (reason.trim().length() < 3){
            sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-report-invalid-length"));
            return;
        }

        if (!sw.existsUser(args[0])) {
            sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-report-invalid-user"));
            return;
        }

        IUser reported = sw.isUserOnline(args[0]) ? sw.getPlayer(args[0]) : sw.getOfflineUser(args[0]);

        if (!reported.isOnline() && !sw.getRConfig().reportOffline()) {
            sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-report-offline-user"));
            return;
        }

        if (sender.getUUID().equals(reported.getUUID())){
            sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-report-invalid-self"));
            return;
        }

        if (reported.hasPermission("reportme.antireport")) {
            sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-report-protected"));
            return;
        }

        if (sameReportCooldown.containsKey(sender.getUUID())) {
            if (sameReportCooldown.get(sender.getUUID()).containsKey(reported.getUUID())) {
                if (sameReportCooldown.get(sender.getUUID()).get(reported.getUUID()) > System.currentTimeMillis()) {
                    sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-report-cooldown-same").replace("{time}",
                            new DecimalFormat("#.0").format((double) (sameReportCooldown.get(sender.getUUID()).get(reported.getUUID()) - System.currentTimeMillis()) / 60000)));
                    return;
                }
                sameReportCooldown.get(sender.getUUID()).remove(reported.getUUID());
            }
            sameReportCooldown.get(sender.getUUID()).put(reported.getUUID(), System.currentTimeMillis() + (sw.getRConfig().getCooldownSame() * 1000));
        } else {
            HashMap<UUID, Long> temp = new HashMap<>();
            temp.put(reported.getUUID(), System.currentTimeMillis() + (sw.getRConfig().getCooldownSame() * 1000));
            sameReportCooldown.put(sender.getUUID(), temp);
        }

        cooldown.put(sender.getUUID(), System.currentTimeMillis() + (sw.getRConfig().getCooldown() * 1000));

        com.andrei1058.reportme.report.Report r = new com.andrei1058.reportme.report.Report(sender, reported, reason, sw.getRConfig().isMergeReports());
        sw.runAsync(() -> {
            int reportId = r.save();
            sender.sendMessages(sw.getMessages().getMessages(sender, "cmd-report-success"));
            if (sw.getRConfig().isDatabaseEnabled() && sw.getRConfig().isWebPanel()) {
                sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-report-success-web").replace("{link}", sw.getRConfig().panelLink() + "?report=" + reportId));
            }

            sw.notifyModerators(r, sw.getRConfig().suspectLevel());
        });
    }
}
