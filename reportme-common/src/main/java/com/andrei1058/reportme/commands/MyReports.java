package com.andrei1058.reportme.commands;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.ReportsManager;
import com.andrei1058.reportme.database.Database;
import com.andrei1058.reportme.report.ReportPreview;
import com.andrei1058.reportme.server.IServerSoftware;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;

public class MyReports implements ICommand {

    @Override
    public String getName() {
        return "myreports";
    }

    @Override
    public void execute(IUser sender, String[] args) {
        IServerSoftware sw = ReportsManager.getReportsManager().getServerSoftware();
        sw.runAsync(() -> {
            if (args.length < 1) {
                sender.sendMessages(sw.getMessages().getMessages(sender, "cmd-myreports-usage"));
                return;
            }

            Database.ReportStatus filter;
            if (args[0].equalsIgnoreCase("a") || args[0].equalsIgnoreCase("active")) {
                filter = Database.ReportStatus.ACTIVE;
            } else if (args[0].equalsIgnoreCase("c") || args[0].equalsIgnoreCase("closed")) {
                filter = Database.ReportStatus.CLOSED;
            } else {
                sender.sendMessages(sw.getMessages().getMessages(sender, "cmd-myreports-usage"));
                return;
            }

            Database.Order order;
            if (args.length > 1) {
                if (args[1].equalsIgnoreCase("l") || args[1].equalsIgnoreCase("latest")) {
                    order = Database.Order.TIME_NEWER;
                } else if (args[1].equalsIgnoreCase("o") || args[1].equalsIgnoreCase("oldest")) {
                    order = Database.Order.TIME_OLDER;
                } else {
                    sender.sendMessages(sw.getMessages().getMessages(sender, "cmd-myreports-usage"));
                    return;
                }
            } else {
                order = Database.Order.TIME_NEWER;
            }

            ParamsCache pc = ParamsCache.getByUUID(sender.getUUID(), order, filter);

            if (filter != pc.getFilter() || order != pc.getOrder()) pc.reCreate(order, filter);
            if (pc.isOutDated()) pc.reCreate(order, filter);

            LinkedList<ReportPreview> myReports = ReportsManager.getReportsManager().database.getMyReports(sender, pc);
            //show status 2 first
            Collections.sort(myReports);

            if (myReports.isEmpty()) {
                sender.sendMessage(sw.getMessages().getMessage(sender, "nothing-to-show"));
                return;
            }

            sender.sendMessage(sw.getMessages().getMessage(sender, filter == Database.ReportStatus.ACTIVE ? "cmd-myreports-active-list-header" : "cmd-myreports-closed-list-header").replace("{page}", String.valueOf(pc.getStart()/sw.getRConfig().getReportsPerPage()+1)));
            String layout = sw.getMessages().getMessage(sender, filter == Database.ReportStatus.ACTIVE ? "cmd-myreports-active-list-format" : "cmd-myreports-closed-list-format");
            SimpleDateFormat dateFormat = new SimpleDateFormat(sw.getMessages().getMessage(sender, "date-format"));

            int reportsPerPage = 0;

            for (ReportPreview rp : myReports) {
                reportsPerPage++;
                if (reportsPerPage > sw.getRConfig().getReportsPerPage()){
                    sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-myreports-next-page"));
                    pc.nextPage();
                    break;
                }
                String rName = rp.getReportedName();
                if (rp.getStatus() == 1 || rp.getStatus() == 2 || rp.getStatus() == 3) {
                    sender.sendMessage(layout.replace("{id}", String.valueOf(rp.getId())).replace("{reported}", rName == null ? rp.getReportedName() : rName)
                                    .replace("{verdict}", rp.getVerdict() == null ? "" : rp.getVerdict()).replace("{unread}", rp.getUnreadMessages() == 0 ? "" : sw.getMessages().getMessage(sender, "placeholder-unread"))
                                    .replace("{reason}", rp.getReason()).replace("{status}", sw.getMessages().getMessage(sender, "placeholder-status-" + rp.getStatus())),
                            sw.getMessages().getMessage(sender, "cmd-myreports-active-hover-format").replace("{reason}", rp.getReason()).replace("{openDate}", dateFormat.format(rp.getOpenDate())), "/rinfo " + rp.getId());
                } else {
                    String mName = rp.getModerator() == null ? rp.getModeratorName() : sw.getName(rp.getModerator()) == null ? rp.getModeratorName() : sw.getName(rp.getModerator());
                    sender.sendMessage(layout.replace("{id}", String.valueOf(rp.getId())).replace("{reported}", rName == null ? rp.getReportedName() : rName)
                                    .replace("{verdict}", rp.getVerdict() == null ? "" : rp.getVerdict()).replace("{unread}", rp.getUnreadMessages() == 0 ? "" : sw.getMessages().getMessage(sender, "placeholder-unread"))
                                    .replace("{reason}", rp.getReason()).replace("{status}", sw.getMessages().getMessage(sender, "placeholder-status-" + rp.getStatus())),
                            sw.getMessages().getMessage(sender, "cmd-myreports-closed-hover-format").replace("{verdict}", rp.getVerdict()).replace("{moderator}", mName).replace("{reason}", rp.getReason()).replace("{closeDate}", dateFormat.format(rp.getCloseDate())), "/rinfo " + rp.getId());
                }
            }
        });
    }

    public static class ParamsCache {

        private int start = 0;
        private Long update;
        private Database.Order order;
        private Database.ReportStatus filter;

        private static HashMap<UUID, ParamsCache> paramsCaches = new HashMap<>();

        public ParamsCache(UUID uuid, Database.Order order, Database.ReportStatus filter) {
            if (paramsCaches.containsKey(uuid)) return;
            update = System.currentTimeMillis() + 15000;
            this.order = order;
            this.filter = filter;
            paramsCaches.put(uuid, this);
        }

        public Database.Order getOrder() {
            return order;
        }

        public Database.ReportStatus getFilter() {
            return filter;
        }

        public void reCreate(Database.Order order, Database.ReportStatus filter) {
            this.order = order;
            this.filter = filter;
            update = System.currentTimeMillis() + 15000;
            start = 0;
        }

        public void nextPage() {
            this.start += ReportsManager.getReportsManager().getServerSoftware().getRConfig().getReportsPerPage();
            update = System.currentTimeMillis() + 15000;
        }

        public int getStart() {
            return start;
        }

        boolean isOutDated() {
            return update < System.currentTimeMillis();
        }

        public static ParamsCache getByUUID(UUID uuid, Database.Order order, Database.ReportStatus filter) {
            return paramsCaches.getOrDefault(uuid, new ParamsCache(uuid, order, filter));
        }
    }
}
