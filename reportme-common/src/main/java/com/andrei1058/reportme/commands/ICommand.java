package com.andrei1058.reportme.commands;

import com.andrei1058.reportme.IUser;

public interface ICommand {

    /**
     * Get command name.
     */
    String getName();

    /**
     * Execute command.
     */
    void execute(IUser sender, String[] args);
}
