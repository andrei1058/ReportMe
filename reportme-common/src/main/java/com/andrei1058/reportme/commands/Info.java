package com.andrei1058.reportme.commands;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.ReportsManager;
import com.andrei1058.reportme.database.Database;
import com.andrei1058.reportme.server.IServerSoftware;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class Info implements ICommand {

    @Override
    public String getName() {
        return "reportinfo";
    }

    @Override
    public void execute(IUser sender, String[] args) {
        IServerSoftware sw = ReportsManager.getReportsManager().getServerSoftware();
        sw.runAsync(() -> {
            if (args.length < 1) {
                sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-rinfo-usage"));
                return;
            }

            try {
                Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-rinfo-usage"));
                return;
            }

            ResultSet r = ReportsManager.getReportsManager().database.getReport(Integer.parseInt(args[0]));

            try {
                if (!r.next()) {
                    sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-rinfo-invalid").replace("{id}", args[0]));
                    return;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-rinfo-invalid").replace("{id}", args[0]));
                try {
                    r.close();
                } catch (SQLException ignored) {
                }
                return;
            }

            UUID reporter = null, moderator = null, reported = null;
            try {
                reporter = UUID.fromString(r.getString("reporter_uuid"));
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (reporter == null) return;

            if (!(reporter.equals(sender.getUUID()) || sender.hasPermission("reportme.*", "reportme.info"))) {
                sender.sendMessage(sw.getMessages().getMessage(sender, "cmd-no-permission"));
                return;
            }

            try {
                if (r.getString("moderator_uuid") != null) {
                    moderator = UUID.fromString(r.getString("moderator_uuid"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                reported = UUID.fromString(r.getString("reported_uuid"));
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (reported == null) return;
            int reportID;
            try {
                reportID = r.getInt("id");
            } catch (SQLException e) {
                e.printStackTrace();
                return;
            }

            // mark report as read
            if (reporter.equals(sender.getUUID())){
                try {
                    if (r.getInt("unread_messages") != 0)
                        ReportsManager.getReportsManager().database.markRead(reportID);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            int activeReports = ReportsManager.getReportsManager().database.getReportedTimes(reported, Database.ReportStatus.ACTIVE);

            try {
                sender.sendReportInfo(reportID, r.getTimestamp("open_date"), r.getInt("status"), r.getString("reporter_name"), reporter,
                        r.getString("reported_name"), reported, r.getString("reason"), r.getString("server"), r.getString("verdict"), r.getString("moderator_name"),
                        moderator, r.getTimestamp("close_date"), ReportsManager.getReportsManager().database.getComments(reportID), activeReports);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                r.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }
}
