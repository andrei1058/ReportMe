package com.andrei1058.reportme.server;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.report.Report;
import com.andrei1058.reportme.commands.ICommand;
import com.andrei1058.reportme.configuration.RConfig;
import com.andrei1058.reportme.configuration.RMessages;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.UUID;

public interface IServerSoftware {

    /**
     * Register a command.
     */
    void registerCommand(ICommand command);

    /**
     * Get configuration.
     */
    RConfig getRConfig();

    /**
     * Get messages.
     */
    RMessages getMessages();

    /**
     * Log an error message.
     */
    void logError(String message);

    /**
     * Run task async.
     */
    void runAsync(Runnable runnable);

    /**
     * Get plugin folder.
     */
    File getPluginFolder();

    /**
     * Check if user exists.
     */
    boolean existsUser(String name);

    /**
     * Check if the player is online.
     */
    boolean isUserOnline(String name);

    /**
     * Get offline user.
     */
    IUser getOfflineUser(String name);

    /**
     * Get player.
     */
    IUser getPlayer(String name);

    /**
     * Send report notification to moderators.
     */
    void notifyModerators(Report report, int suspectLevel);

    /**
     * Server name.
     */
    String getServerName();

    /**
     * Get name of player by uuid.
     *
     * @return null if not found.
     */
    @Nullable
    String getName(UUID uuid);
}
