package com.andrei1058.reportme.report;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.ReportsManager;

public class Report {

    private IUser reporter, reported;
    private String reason;
    private boolean merge;
    private int reportedTimes;

    public Report(IUser reporter, IUser reported, String reason, boolean merge) {
        this.reported = reported;
        this.reporter = reporter;
        this.reason = reason;
        this.merge = merge;
    }


    /**
     * Save report to database.
     *
     * @return report id;
     */
    public synchronized int save() {
        int reportId = -1;

        if (merge) {
            reportId = ReportsManager.getReportsManager().database.getExisting(this);
        }

        if (reportId == -1) {
            reportId = ReportsManager.getReportsManager().database.saveNewReport(this);
        } else {
            if (merge) {
                ReportsManager.getReportsManager().database.commentReport(reportId, getReporter(), getReason());
            } else {
                reportId = ReportsManager.getReportsManager().database.saveNewReport(this);
            }
        }

        reportedTimes = ReportsManager.getReportsManager().database.increaseReportTimes(getReported());
        return reportId;
    }

    public String getReason() {
        return reason;
    }

    /**
     * Used for suspect level.
     */
    public int getReportedTimes() {
        return reportedTimes;
    }

    public IUser getReported() {
        return reported;
    }

    public IUser getReporter() {
        return reporter;
    }
}
