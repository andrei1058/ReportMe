package com.andrei1058.reportme.report;

import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.util.UUID;

public class ReportPreview implements Comparable<ReportPreview> {

    private int id, status;
    private UUID reported, moderator = null;
    private String reportedName, verdict, reason, moderatorName;
    private Timestamp openDate, closeDate, lastMessage;
    private int unreadMessages;

    public ReportPreview(int id, int status, String reported, String reason, String reportedName, String verdict, Timestamp openDate, Timestamp closeDate, int unreadMessages, Timestamp lastMessage, String moderator, String moderatorName) {
        this.id = id;
        this.status = status;
        this.reported = UUID.fromString(reported);
        this.reportedName = reportedName;
        this.openDate = openDate;
        this.closeDate = closeDate;
        this.verdict = verdict;
        this.unreadMessages = unreadMessages;
        this.lastMessage = lastMessage;
        this.reason = reason;
        try {
            this.moderator = UUID.fromString(moderator);
        } catch (Exception ignored) {
        }
        ;
        this.moderatorName = moderatorName;
    }

    public int getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }

    public String getReportedName() {
        return reportedName;
    }

    public String getVerdict() {
        return verdict;
    }

    public UUID getReported() {
        return reported;
    }

    public int getUnreadMessages() {
        return unreadMessages;
    }

    public Timestamp getCloseDate() {
        return closeDate;
    }

    public Timestamp getOpenDate() {
        return openDate;
    }

    public Timestamp getLastMessage() {
        return lastMessage;
    }

    public String getReason() {
        return reason;
    }

    public UUID getModerator() {
        return moderator;
    }

    public String getModeratorName() {
        return moderatorName;
    }

    @Override
    public int compareTo(@NotNull ReportPreview o) {
        return o.getStatus() == 2 && getStatus() == 2 ? 0 : (o.getStatus() == 2 && getStatus() != 2) ? 1 : -1;
    }
}
