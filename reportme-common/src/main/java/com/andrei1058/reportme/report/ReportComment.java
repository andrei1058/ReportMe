package com.andrei1058.reportme.report;

import java.sql.Timestamp;
import java.util.UUID;

public class ReportComment {

    private Timestamp date;
    private String user_name, message;
    private UUID user;
    private boolean moderator;
    public ReportComment(Timestamp date, String user_name, String user_uuid, int moderator, String message) {
        this.date = date;
        this.user_name = user_name;
        this.user = UUID.fromString(user_uuid);
        this.moderator = moderator == 1;
        this.message = message;
    }

    public Timestamp getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public String getUsername() {
        return user_name;
    }

    public UUID getUser() {
        return user;
    }

    public boolean isModerator() {
        return moderator;
    }

    public void setUsername(String user_name) {
        this.user_name = user_name;
    }
}
