package com.andrei1058.reportme.configuration;

import com.andrei1058.reportme.IUser;
import java.util.List;

public interface RMessages {

    /**
     * Get message for user.
     *
     * @param path is the message path.
     * @param user useful for future per player language support
     */
    String getMessage(IUser user, String path);

    /**
     * Get a list of messages.
     *
     * @param path is the message path.
     * @param user useful for future per player language support.
     */
    List<String> getMessages(IUser user, String path);
}
