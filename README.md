### What is this?
**ReportMe** is a cross platform plugin for Minecraft servers. It is a simple and efficient reports manager.

Supported server software: Spigot (and forks), Nukkit, Bungeecord (and forks) and Sponge.

![preview](https://i.imgur.com/VsT5wib.png)

### Compatibility

###### Bukkit/ Spigot/ Paper
  - 1.14.x (v1_14_R1)
  - 1.13.x (v1_13_R1, v1_13_R2)
  - 1.12.x (v1_12_R1)
  - 1.11.x (v1_11_R1)
  - 1.10.x (v1_10_R1)
  - 1.9.x (v1_9_R1, v1_9_R2)
  - 1.8.x (v1_8_R1, v1_8_R2, v1_8_R3)
  - 1.7.x (v1_7_R1, v1_7_R2, v1_7_R3, v1_7_R4)
  - older versions should work as well but they were not tested