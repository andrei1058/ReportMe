package com.andrei1058.reportme.server.bukkit.richchat;

import com.andrei1058.reportme.report.Report;
import com.andrei1058.reportme.server.bukkit.LanguageBukkit;
import org.bukkit.entity.Player;

public class BukkitChat extends ChatFormat {

    @Override
    public void sendNewReportMessage(Player player, Report report) {
        for (String s : LanguageBukkit.getMessages(player, "moderator-notify-chat")) {
            player.sendMessage(s.replace("{reporter}", report.getReporter().getName()).replace("{reported}", report.getReported().getName()).replace("{reason}", report.getReason()));
        }
    }

    @Override
    public void sendMsg(Player player, String msg, String hover, String click) {
        player.sendMessage(msg);
    }
}
