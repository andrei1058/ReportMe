package com.andrei1058.reportme.server;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.report.Report;
import com.andrei1058.reportme.ReportsManager;
import com.andrei1058.reportme.commands.ICommand;
import com.andrei1058.reportme.configuration.RConfig;
import com.andrei1058.reportme.configuration.RMessages;
import com.andrei1058.reportme.server.bukkit.ConfigBukkit;
import com.andrei1058.reportme.server.bukkit.LanguageBukkit;
import com.andrei1058.reportme.server.bukkit.UserBukkit;
import com.andrei1058.reportme.server.bukkit.richchat.BukkitChat;
import com.andrei1058.reportme.server.bukkit.richchat.ChatFormat;
import com.andrei1058.reportme.server.bukkit.richchat.SpigotChat;
import com.andrei1058.reportme.server.bukkit.versionsupport.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import java.util.logging.Level;

public class ReportMeBukkit extends JavaPlugin implements IServerSoftware, CommandExecutor {

    public static Plugin plugin;
    private static VersionSupport versionSupport;
    public static ChatFormat chatFormat = new BukkitChat();
    private static String serverVersion;

    @Override
    public void onEnable() {
        plugin = this;

        serverVersion = Bukkit.getServer().getClass().getName().split("\\.")[3];

        try {
            Class supp = Class.forName("com.andrei1058.reportme.server.bukkit.versionsupport." + serverVersion);
            Constructor c = supp.getConstructors()[0];
            versionSupport = (VersionSupport) c.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            versionSupport = new OldVersion();
            getLogger().log(Level.WARNING, "Loading legacy support for server version: " + serverVersion);
            getLogger().log(Level.WARNING, "Titles, Sub-Titles and Action messages will not work.");
        }

        try {
            Class.forName("org.spigotmc.SpigotConfig");
            chatFormat = new SpigotChat();
        } catch (ClassNotFoundException ignored) {
        }

        ConfigBukkit.init();
        ReportsManager.init(this);
        LanguageBukkit.init();

        new MetricsLite(this);
    }

    @Override
    public void onDisable() {
        ReportsManager.getReportsManager().database.close();
    }

    @Override
    public void registerCommand(ICommand command) {
        getCommand(command.getName()).setExecutor(this);
    }

    @Override
    public RConfig getRConfig() {
        return ConfigBukkit.getConfigBukkit();
    }

    @Override
    public RMessages getMessages() {
        return LanguageBukkit.getMessages();
    }

    @Override
    public void logError(String message) {
        this.getLogger().severe(message);
    }

    @Override
    public void runAsync(Runnable runnable) {
        Bukkit.getScheduler().runTaskAsynchronously(this, runnable);
    }

    @Override
    public File getPluginFolder() {
        return this.getDataFolder();
    }

    @Override
    public boolean existsUser(String name) {
        return Bukkit.getPlayer(name) != null;
    }

    @Override
    public boolean isUserOnline(String name) {
        return Bukkit.getPlayer(name).isOnline();
    }

    @Override
    public IUser getOfflineUser(String name) {
        return new UserBukkit(Bukkit.getPlayer(name));
    }

    @Override
    public IUser getPlayer(String name) {
        return new UserBukkit(Bukkit.getPlayer(name));
    }

    @Override
    public void notifyModerators(Report report, int suspectLevel) {
        versionSupport.sendTitle(Bukkit.getPlayer(report.getReporter().getUUID()), null, getMessages().getMessage(report.getReporter(), "user-notify-subtitle"), 0, 30, 0);

        // send to moderators
        if (report.getReportedTimes() >= suspectLevel) {
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (!p.hasPermission("reportme.moderator")) continue;
                chatFormat.sendNewReportMessage(p, report);
                versionSupport.sendTitle(p, LanguageBukkit.getMessage(p, "moderator-notify-title").replace("{reported}", report.getReported().getName()).replace("{reporter}", report.getReporter().getName()),
                        LanguageBukkit.getMessage(p, "moderator-notify-subtitle").replace("{reported}", report.getReported().getName()).replace("{reporter}", report.getReporter().getName()), 0, 70, 0);
                versionSupport.playAction(p, LanguageBukkit.getMessage(p, "moderator-notify-action"));
            }
        } else {
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (!p.hasPermission("reportme.moderator")) continue;
                // skip suspects only
                if (ReportsManager.getReportsManager().getSuspectsOnly().contains(p.getUniqueId())) continue;
                chatFormat.sendNewReportMessage(p, report);
                versionSupport.sendTitle(p, LanguageBukkit.getMessage(p, "moderator-notify-title").replace("{reported}", report.getReported().getName()).replace("{reporter}", report.getReporter().getName()),
                        LanguageBukkit.getMessage(p, "moderator-notify-subtitle").replace("{reported}", report.getReported().getName()).replace("{reporter}", report.getReporter().getName()), 0, 70, 0);
                versionSupport.playAction(p, LanguageBukkit.getMessage(p, "moderator-notify-action"));
            }
        }
    }

    @Override
    public String getServerName() {
        return getConfig().getString("server-name");
    }

    @Override
    public @Nullable String getName(UUID uuid) {
        return Bukkit.getPlayer(uuid) == null ? null : Bukkit.getPlayer(uuid).getName();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        for (ICommand c : ReportsManager.getReportsManager().getCommands()) {
            if (command.getName().equalsIgnoreCase(c.getName())) {
                c.execute(new UserBukkit(((Player) sender)), args);
                return true;
            }
        }
        return false;
    }

    public static String getServerVersion() {
        return serverVersion;
    }
}
