package com.andrei1058.reportme.server.bukkit.richchat;

import com.andrei1058.reportme.report.Report;
import com.andrei1058.reportme.server.bukkit.LanguageBukkit;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

public class SpigotChat extends ChatFormat {
    @Override
    public void sendNewReportMessage(Player player, Report report) {
        for (String s : LanguageBukkit.getMessages(player, "moderator-notify-chat")) {
            player.spigot().sendMessage(createTc(report, s, player));
        }
    }

    @Override
    public void sendMsg(Player player, String msg, String hover, String click) {
        TextComponent tc = new TextComponent(msg);
        tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hover).create()));
        tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, click));
        player.spigot().sendMessage(tc);
    }

    private TextComponent createTc(Report report, String s, Player player) {
        TextComponent tc = new TextComponent(s.replace("{reporter}", report.getReporter().getName()).replace("{reported}", report.getReported().getName()).replace("{reason}", report.getReason()));
        tc.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "tp " + report.getReported().getName()));
        tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(LanguageBukkit.getMessage(player, "moderator-notify-chat-hover")).create()));


        return tc;
    }
}
