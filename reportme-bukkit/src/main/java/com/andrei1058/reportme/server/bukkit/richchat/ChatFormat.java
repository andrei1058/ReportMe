package com.andrei1058.reportme.server.bukkit.richchat;


import com.andrei1058.reportme.report.Report;
import org.bukkit.entity.Player;

public abstract class ChatFormat {

    /**
     * Send rich text.
     */
    public abstract void sendNewReportMessage(Player player, Report report);

    public abstract void sendMsg(Player player, String msg, String hover, String click);
}
