package com.andrei1058.reportme.server.bukkit;

import com.andrei1058.reportme.configuration.RConfig;
import com.andrei1058.reportme.server.ReportMeBukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ConfigBukkit implements RConfig {

    private static ConfigBukkit configBukkit = null;

    private YamlConfiguration yml;
    private File file;

    private ConfigBukkit() {
        configBukkit = this;

        if (!ReportMeBukkit.plugin.getDataFolder().exists()) {
            if (!ReportMeBukkit.plugin.getDataFolder().mkdir())
                ReportMeBukkit.plugin.getLogger().severe("Could not create " + ReportMeBukkit.plugin.getDataFolder().getPath());
        }

        file = new File(ReportMeBukkit.plugin.getDataFolder(), "bukkit_config.yml");
        if (!file.exists()) {
            try {
                if (!file.createNewFile())
                    ReportMeBukkit.plugin.getLogger().severe("Could not create " + file.getPath());
            } catch (IOException e) {
                ReportMeBukkit.plugin.getLogger().severe("Could not create " + file.getPath());
                e.printStackTrace();
                return;
            }
        }

        yml = YamlConfiguration.loadConfiguration(file);

        yml.options().copyDefaults(true);
        yml.addDefault("default-language", "en");
        yml.addDefault("database.enable", false);
        yml.addDefault("database.host", "localhost");
        yml.addDefault("database.port", 3306);
        yml.addDefault("database.ssl", false);
        yml.addDefault("database.name", "ReportMe");
        yml.addDefault("database.user", "root");
        yml.addDefault("database.pass", "bread");

        yml.addDefault("default-notify-layout", "ALL");
        yml.addDefault("enable-moderator-motd", true);
        yml.addDefault("report-cooldown", 30);
        yml.addDefault("same-player-report-cooldown", 2400);
        yml.addDefault("merge-same-player-reports", true);
        yml.addDefault("suspect-at-amount", 5);
        yml.addDefault("reports-per-page", 7);
        yml.addDefault("teleport-on-click", true);
        yml.addDefault("report-offline-players", false);
        yml.addDefault("disabled-servers", Collections.singletonList("server name here"));
        yml.addDefault("web-panel.active", true);
        yml.addDefault("web-panel.link", "https://andrei1058.com/reports");
        yml.addDefault("accept-custom-reason", true);
        yml.addDefault("server-name", "bukkitServer");
        yml.addDefault("report-reason-list", Arrays.asList("cheating","hack", "killaura", "fly"));
        save();
    }

    /**
     * Save changes.
     */
    private void save() {
        try {
            yml.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initialize configuration.
     */
    public static void init() {
        if (configBukkit == null) configBukkit = new ConfigBukkit();
    }

    public static ConfigBukkit getConfigBukkit() {
        return configBukkit;
    }

    @Override
    public String getDefaultLanguage() {
        return yml.getString("default-language");
    }

    @Override
    public boolean isDatabaseEnabled() {
        return yml.getBoolean("database.enable");
    }

    @Override
    public String getDatabaseHost() {
        return yml.getString("database.host");
    }

    @Override
    public String getDatabaseName() {
        return yml.getString("database.name");
    }

    @Override
    public String getDatabaseUser() {
        return yml.getString("database.user");
    }

    @Override
    public String getDatabasePassword() {
        return yml.getString("database.pass");
    }

    @Override
    public String getDatabasePort() {
        return String.valueOf(yml.getInt("database.port"));
    }

    @Override
    public boolean isDatabaseSSL() {
        return yml.getBoolean("database.ssl");
    }

    @Override
    public boolean reportOffline() {
        return yml.getBoolean("report-offline-players");
    }

    @Override
    public boolean isWebPanel() {
        return yml.getBoolean("web-panel.active");
    }

    @Override
    public String panelLink() {
        return yml.getString("web-panel.link");
    }

    @Override
    public int suspectLevel() {
        return yml.getInt("suspect-at-amount");
    }

    @Override
    public int getCooldown() {
        return yml.getInt("report-cooldown");
    }

    @Override
    public boolean isMergeReports() {
        return yml.getBoolean("merge-same-player-reports");
    }

    @Override
    public int getCooldownSame() {
        return yml.getInt("same-player-report-cooldown");
    }

    @Override
    public boolean acceptCustomReason() {
        return yml.getBoolean("accept-custom-reason");
    }

    @Override
    public List<String> getReportTypes() {
        return yml.getStringList("report-reason-list");
    }

    @Override
    public int getReportsPerPage() {
        return yml.getInt("reports-per-page");
    }
}
