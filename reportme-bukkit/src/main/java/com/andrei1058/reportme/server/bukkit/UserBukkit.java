package com.andrei1058.reportme.server.bukkit;

import com.andrei1058.reportme.IUser;
import com.andrei1058.reportme.report.ReportComment;
import com.andrei1058.reportme.server.ReportMeBukkit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class UserBukkit implements IUser {

    private Player player;

    public UserBukkit(Player player) {
        this.player = player;
    }

    @Override
    public String getName() {
        return player.getName();
    }

    @Override
    public UUID getUUID() {
        return player.getUniqueId();
    }

    @Override
    public void sendMessage(String message) {
        player.sendMessage(message);
    }

    @Override
    public void sendMessage(String message, String hover, String click) {
        ReportMeBukkit.chatFormat.sendMsg(player, message, hover, click);
    }

    @Override
    public void sendMessages(List<String> messages) {
        for (String m : messages) {
            player.sendMessage(m);
        }
    }

    @Override
    public void sendReportInfo(int id, Timestamp openDate, int status, String reporterName, UUID reporter, String reportedName, UUID reported, String reason,
                               String server, String verdict, String moderatorName, UUID moderator, Timestamp closeDate, LinkedList<ReportComment> comments, int activeReports) {
        //active
        SimpleDateFormat dateFormat = new SimpleDateFormat(LanguageBukkit.getMessage(player, "date-format"));
        if (Bukkit.getPlayer(reporter) != null) reporterName = Bukkit.getPlayer(reporter).getName();
        if (Bukkit.getPlayer(reported) != null) reportedName = Bukkit.getPlayer(reported).getName();
        if (status == 1 || status == 2 || status == 3) {
            for (String s : LanguageBukkit.getMessages(player, "cmd-rinfo-status-open")) {
                player.sendMessage(s.replace("{id}", String.valueOf(id)).replace("{openDate}", dateFormat.format(openDate)).replace("{reporter}", reporterName).replace("{reported}", reportedName)
                        .replace("{reason}", reason).replace("{activeReports}", String.valueOf(activeReports)).replace("{status}", LanguageBukkit.getMessage(player, "placeholder-status-" + status)));
            }
            if (!comments.isEmpty()) {
                player.sendMessage(LanguageBukkit.getMessage(player, "cmd-rinfo-comments-header").replace("{id}", String.valueOf(id)));
                for (ReportComment rc : comments) {
                    if (Bukkit.getPlayer(rc.getUser()) != null) {
                        rc.setUsername(Bukkit.getPlayer(rc.getUser()).getName());
                    }
                    player.sendMessage(LanguageBukkit.getMessage(player, (rc.isModerator() ? "cmd-rinfo-comments-format-admin" : "cmd-rinfo-comments-format-user"))
                            .replace("{date}", dateFormat.format(rc.getDate())).replace("{user}", rc.getUsername()).replace("{comment}", rc.getMessage()));
                }
            }
        } else if (status == 0 || status == 4) {
            for (String s : LanguageBukkit.getMessages(player, "cmd-rinfo-status-closed")) {
                player.sendMessage(s.replace("{id}", String.valueOf(id)).replace("{openDate}", dateFormat.format(openDate)).replace("{reporter}", reporterName).replace("{reported}", reportedName)
                        .replace("{reason}", reason).replace("{activeReports}", String.valueOf(activeReports)).replace("{status}", LanguageBukkit.getMessage(player, "placeholder-status-" + status))
                        .replace("{closeDate}", dateFormat.format(closeDate)).replace("{moderator}", moderatorName.isEmpty() ? LanguageBukkit.getMessage(player, "placeholder-moderator") : moderatorName)
                        .replace("{verdict}", verdict));
            }
            if (!comments.isEmpty()) {
                player.sendMessage(LanguageBukkit.getMessage(player, "cmd-rinfo-comments-header").replace("{id}", String.valueOf(id)));
                for (ReportComment rc : comments) {
                    if (Bukkit.getPlayer(rc.getUser()) != null) {
                        rc.setUsername(Bukkit.getPlayer(rc.getUser()).getName());
                    }
                    player.sendMessage(LanguageBukkit.getMessage(player, (rc.isModerator() ? "cmd-rinfo-comments-format-admin" : "cmd-rinfo-comments-format-user"))
                            .replace("{date}", dateFormat.format(rc.getDate())).replace("{user}", rc.getUsername()).replace("{comment}", rc.getMessage()));
                }
            }
        }
    }

    @Override
    public boolean hasPermission(String... permissions) {
        for (String perm : permissions) {
            if (player.hasPermission(perm)) return true;
        }
        return false;
    }

    @Override
    public boolean isOnline() {
        return player.isOnline();
    }
}
