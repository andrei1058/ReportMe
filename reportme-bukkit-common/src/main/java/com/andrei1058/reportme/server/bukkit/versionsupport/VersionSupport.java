package com.andrei1058.reportme.server.bukkit.versionsupport;

import org.bukkit.entity.Player;

public interface VersionSupport {

    void sendTitle(Player p, String title, String subtitle, int fadeIn, int stay, int fadeOut);

    void playAction(Player p, String text);
}
